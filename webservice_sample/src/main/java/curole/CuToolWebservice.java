package curole;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;



@Path("/webservice")
	public class CuToolWebservice {
	@GET
    @Path("/curole")
	@Produces({MediaType.APPLICATION_JSON})
    public Map<String,List<String>> hello(){
		File_Segregation file_Segregation = new File_Segregation();
		Map<String,List<String>> toot_map = new TreeMap<>();
		toot_map = file_Segregation.fileAllocationService("/home/karthik/ML_ProjectSource/Cu_ToolNameFinder/CommanSource/images");
        return toot_map; 
    }
	
	
	@GET
    @Path("/test")
	@Produces("text/plain")
    public String hello1(){
		System.out.println("test");
		String s= "testing";
		System.out.println("sk");
		return "welcome";
    }
    
//	
//	@POST
//    @Path("/test")
//	@Produces("text/plain")
//    public void hello1(){
//		System.out.println("testttt");
//    }
    
    
		@POST
		@Path("/cupath/{demo}")
		@Consumes("text/plain")
		public void demo(@PathParam("demo") String userAgent){
			
			System.out.println(userAgent);
		}
	// parsing json element 
		
//		    @POST
//		    @Path("/order")
//		    @Consumes(MediaType.APPLICATION_JSON)
//		    public Response getUserById(Order inputOrder){
//		  
//		        System.out.println(inputOrder.toString());
//		         
//		        return Response.status(200).entity("Your order is in-progress").build();
//		    }
		    
		 // parsing json element 
			
		    @POST
		    @Path("/orderxml")
		    @Consumes(MediaType.APPLICATION_XML)
		    public Response getUserById1(Order inputOrder){
		        System.out.println(inputOrder.toString());
		        return Response.status(200).entity("Your order is in-progress").build();
		    }
		
}
