package curole;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.TreeMap;
import java.util.stream.Collectors;

public class File_Utils {
	
	File_Entity file_Entity = new File_Entity();

	public void fileValidation(String path) {
		String filepath = path;
		File file = new File(filepath);
		if (file.exists()) {
			file.delete();
		}

	}

	public String regxValidation(String s) {
		String str = s.toLowerCase();
		str = str.replaceAll("[\\W_-[1-9]]", "");
		str = str.replace("unknowntag", "");
		str = str.replace("0x", "img0x");
		return str;
	}

	public void filewriter(String path, String text) {
		FileWriter fw = null;
		try {
			fw = new FileWriter(path, true);
			BufferedWriter bw = new BufferedWriter(fw);
			bw.append(text);
			bw.newLine();
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public List readFilesFromFolder(String path) {
		List<File> filesInFolder = null;
		try {
			filesInFolder = Files.walk(Paths.get(path)).filter(Files::isRegularFile).map(Path::toFile)
					.collect(Collectors.toList());
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return filesInFolder;
	}

	public String getMimeType(String path) {
		
		path = path.toLowerCase();

		String mimeType = null;
		try {

			mimeType = Files.probeContentType(new File(path).toPath());

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return mimeType;
	}

	public String getFileName(String path) {
        String fname = path.toLowerCase();
        fname = fname.replaceAll(" ", "_");
        if (fname.lastIndexOf(".") > 0) {
            int position = fname.lastIndexOf(".");
            fname = fname.substring(0, position);
        }
        return fname;
    }

    public String getFileExtn(String path) {
        String fileName = path;
        String xtn = "";
        int position = fileName.lastIndexOf(".");
        if (position != -1) {
            xtn = fileName.substring(position + 1, fileName.length());
        }
        return xtn;
    }
}