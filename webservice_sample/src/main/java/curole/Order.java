package curole;

import javax.xml.bind.annotation.XmlAccessorOrder;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonProperty;




@XmlRootElement(name = "order1")
public class Order {
	 
   // @JsonProperty
 //   @XmlElement(name="person")
	@XmlElement(nillable=false, required=true)
    private String custmer;
	@XmlElement(nillable=false, required=true)
    private String address;
	@XmlElement(nillable=false, required=true)
	 private String amount;
   
    public String getCustmer() {
        return custmer;
    }
    public void setCustmer(String custmer) {
        this.custmer = custmer;
    }
     
 
    public String getAddress() {
        return address;
    }
    public void setAddress(String address) {
        this.address = address;
    }
     
  
    public String getAmount() {
        return amount;
    }
    public void setAmount(String amount) {
        this.amount = amount;
    }
	@Override
	public String toString() {
		return "Order [custmer=" + custmer + ", address=" + address + ", amount=" + amount + "]";
	}
    
    
}