package curole;

import java.util.Map;
import java.util.TreeMap;

public class File_Entity {
	
	private String fileSourceName;
	private String fileName;
	private String filePath;
	private String fileExtn;
	private String mimeType;
	private String fileRolle;
	private Map<String,Object> property = new TreeMap();
	
	
	public String getFileSourceName() {
		return fileSourceName;
	}
	public void setFileSourceName(String fileSourceName) {
		this.fileSourceName = fileSourceName;
	}
	public String getFileRolle() {
		return fileRolle;
	}
	public void setFileRolle(String fileRolle) {
		this.fileRolle = fileRolle;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getFilePath() {
		return filePath;
	}
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	public String getFileExtn() {
		return fileExtn;
	}
	public void setFileExtn(String fileExtn) {
		this.fileExtn = fileExtn;
	}
	public String getMimeType() {
		return mimeType;
	}
	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}
	public Map<String,Object> getProperty() {
		return property;
	}
	public void setProperty(Map<String,Object> property) {
		this.property = property;
	}
	
}
