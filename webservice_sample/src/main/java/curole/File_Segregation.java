package curole;

import java.io.File;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class File_Segregation {
	
	
	public Map<String ,List<String>> fileAllocationService(String root){
		
		File_Utils file_Utils  = new File_Utils();
		Map<String,List<String>> toolMap = new TreeMap<>();
		String path = root;
		List<File> fileList = file_Utils.readFilesFromFolder(path);
		for(int i=0;i<fileList.size();i++){
			List<String> list = new ArrayList<>();
			
			File_Entity entity = new File_Entity();
			entity.setFilePath(path);
			entity.setFileSourceName(fileList.get(i).getName());
			entity.setFileExtn(fileList.get(i).getName());
			
			if(entity.getFileExtn().contains(".doc")){
				if(toolMap.containsKey("astool")){
					list .addAll(toolMap.get("astool"));
					list.add(fileList.get(i).getName());
					toolMap.put("astool", list);
				} else {
					toolMap.put("astool", list);
				}
			}
			
			else if(entity.getFileExtn().contains(".jpeg")){
				
				if(toolMap.containsKey("imagetool")){
					list .addAll(toolMap.get("imagetool"));
					list.add(fileList.get(i).getName());
					toolMap.put("imagetool", list);
				} else {
					toolMap.put("imagetool", list);
				}
				
			} else {
				if(toolMap.containsKey("othertool")){
					list .addAll(toolMap.get("othertool"));
					list.add(fileList.get(i).getName());
					toolMap.put("othertool", list);
				} else {
					toolMap.put("othertool", list);
				}
			}
		}		
		return toolMap;
	}
}


