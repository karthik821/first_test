package cutool_zookeeper;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.ZooDefs.Ids;
import org.apache.zookeeper.ZooKeeper;

public class ZookeeperService {

	
	public static class ZooKeeperWatcher implements Watcher {
		
	    public void process(WatchedEvent event) {
	      
	    	System.out.println("watching event accour");
	    }
	}
	
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		final ZooKeeper zooKeeper = new ZooKeeper(" 172.16.74.150:2181", 2000,new ZooKeeperWatcher());
	    
	    try {
	      //create a node with path "/demo", data as blank, ACL giving everyone rights on this node and create mode as Ephemeral
	      final String createdNodePath = zooKeeper.create("/demo", new byte[0], Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT);
	      
	     // final String createdNodePath1 = zooKeeper.create("/demo1", new byte[0], Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT);

	    
	      
	      //Delete a node with path "/demo" and version as -1 means don't care for current version
	      zooKeeper.delete("/demo", -1);
	      
	    } catch (KeeperException | InterruptedException e) {
	      e.printStackTrace();
	    }

	}
}
